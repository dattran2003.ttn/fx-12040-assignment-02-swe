# FUNiX Passport
## Thông tin dự án
- Tên dự án: Quản lý mã nguồn Funix Passport
- Họ và tên sinh viên: Trần Mạnh Đạt
- MSSV: FX12040

## Cấu trúc repository
- Mục đích: Repository này sử dụng để quản lý phiên bản mã nguồn cho dự án Funix Passport
- Các branch:
    1. Branch master là branch chính. 
    2. Branch document cập nhật các tài liệu về dự án như đặc tả SRS, hình ảnh các biểu đồ Usecase, Activity, Sequence Diagram. 
    3. Branch backend cập nhật file backend.md mô tả vai trò, mục đích và các chức năng của backend.
    4. Branch feat/enable_auto_subtitle cập nhật tính năng tự động hiển thị phụ đề video.
    5. Branch bug/clear_console_log để xoá các hàm console trong chương trình.